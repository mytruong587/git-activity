class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :lastname
      t.string :firstname
      t.integer :phonenum
      t.string :email

      t.timestamps
    end
  end
end
